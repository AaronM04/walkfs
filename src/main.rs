use anyhow as A;
use std::cmp;
use std::collections::BinaryHeap;
use std::time::SystemTime;
use time as T;
use walkdir as W;
#[allow(unused)]
use A::Context;

/// Describes a file's path, size, and modified time
#[derive(Debug, Clone, Eq)]
struct Item(String, u64, SystemTime);

impl Ord for Item {
    fn cmp(&self, other: &Item) -> cmp::Ordering {
        let mtime_cmp = self.2.cmp(&other.2);
        if mtime_cmp != cmp::Ordering::Equal {
            return mtime_cmp;
        }
        self.0.cmp(&other.0) // path comparison
    }
}

impl PartialOrd for Item {
    fn partial_cmp(&self, other: &Item) -> Option<cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Item {
    fn eq(&self, other: &Item) -> bool {
        self.0 == other.0 && self.2 == other.2
    }
}

fn main() -> A::Result<()> {
    let mut item_heap = collect_and_sort_items("/", |direntry| {
        let md = direntry.metadata();
        if let Err(e) = md {
            println!("ERR in filter: {:?}", e);
            return false;
        }
        let md = md.unwrap(); // unwrap OK because of above check
        let path = direntry.path();
        !(
            // Exclude mounted filesystems other than /
            path.starts_with("/home")
                || path.starts_with("/dev")
                || path.starts_with("/run")
                || path.starts_with("/boot")
                || path.starts_with("/var/lib/snapd/snap")
                || path.starts_with("/run/user/1000")
                || path.starts_with("/tmp")
        ) && (md.is_dir() || md.len() > 5_000_000)
    })?;
    while let Some(item) = item_heap.pop() {
        println!("  {:?}", item);
    }
    Ok(())
}

/// Walks filesystem under specified path, collecting Items and sorting by modified time (most
/// recent first).
fn collect_and_sort_items(
    path: &str,
    filter: impl FnMut(&W::DirEntry) -> bool,
) -> A::Result<BinaryHeap<Item>> {
    let mut items = BinaryHeap::new();
    for entry in W::WalkDir::new(path).into_iter().filter_entry(filter) {
        if let Err(e) = entry {
            let e = A::Error::from(e).context("getting direntry");
            println!("ERR: {:?}", e);
            continue;
        }
        let direntry = entry.unwrap(); // unwrap OK because of above test
        let md = direntry.metadata()?;
        if md.is_dir() {
            continue;
        }
        if direntry.path_is_symlink() {
            continue;
        }
        let mtime = md.modified()?;
        let item = Item(
            direntry.path().to_str().unwrap().to_owned(),
            md.len(),
            mtime,
        );
        items.push(item);
    }
    Ok(items)
}
